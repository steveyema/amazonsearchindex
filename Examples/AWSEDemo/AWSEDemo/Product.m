//
//  Product.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "Product.h"

@implementation Product

-(id)initWithItem:(Item *)item andURL:(NSURL *)url
{
    if (self = [super initWithUrl:url])
    {
        _item = item;
    }
    return self;
}

@end
