//
//  ProductTableViewCell.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductTableViewCell.h"

@interface ProductTableViewCell()


@end

@implementation ProductTableViewCell

+ (NSString *)unqiueIdentifier
{
    return @"ProductTableViewCellIdentifier";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
