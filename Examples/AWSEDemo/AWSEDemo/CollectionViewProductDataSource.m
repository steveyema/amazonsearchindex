//
//  CollectionViewProductDataSource.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "CollectionViewProductDataSource.h"
#import "ProductCollectionViewCell.h"
#import "CommonTypes.h"
#import "Price+Currency.h"
#import "CurrencyController.h"
#import "SettingController.h"
#import "Product.h"

@interface CollectionViewProductDataSource()

@property(nonatomic, strong) UICollectionView *collectionView;

@end

@implementation CollectionViewProductDataSource

- (id)initWithCollectionView:(UICollectionView *)collectionView
{
    if (self = [super init])
    {
        _collectionView = collectionView;
        [collectionView registerNib:[UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:[ProductCollectionViewCell unqiueIdentifier]];
        collectionView.delegate = self;
        collectionView.dataSource = self;
    }
    return self;
}

#pragma mark - UICollectionDelegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCollectionViewCell *cell = (ProductCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[ProductCollectionViewCell unqiueIdentifier] forIndexPath:indexPath];
    
    [self setCell:cell atIndexPath:indexPath];

    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.items? self.items.count : 0;
}

- (void)setCell:(ProductCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Product *product = (Product *)[self.items objectAtIndex:indexPath.row];
    cell.productName.text = product.item.itemAttributes.title;
    
    Price *price = product.item.itemAttributes.listPrice;
    cell.productPrice.text = [price formattedPriceForCurrencyConversion:[[CurrencyController sharedInstance] currentRateFromUSDTo:[SettingController sharedInstance].currency]];
    
    cell.imageView.image = product.image;
    
    switch (product.state) {
        case PhotoRecordStateDownloaded:
        {
            break;
        }
        case PhotoRecordStateFailed:
        {
            break;
        }
        default:
        {
            if (!self.collectionView.dragging && !self.collectionView.decelerating)
            {
                [self startOperationsForPhotoRecord:product atIndexPath:indexPath];
            }
            break;
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL isPortrait = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]);
    
    return CGSizeMake(isPortrait? (collectionView.frame.size.width / 3 - 10) : (collectionView.frame.size.height / 3 - 10), 170);
}

#pragma mark - abstract implementation
- (void)updateUI
{
    [self.collectionView reloadData];
}

- (void)reloadRowAtIndexPath:(NSIndexPath *)indexPath
{
    //do not implement
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (NSURL *)imageUrl:(Item *)item
{
    return [NSURL URLWithString:item.mediumImage.url];
}

@end
