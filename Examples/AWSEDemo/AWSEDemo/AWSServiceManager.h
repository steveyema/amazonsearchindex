//
//  AWSServiceManager.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AWSServiceManagerDelegate <NSObject>

- (void)readyWithListOfItems:(NSArray *)items;

@end

@interface AWSServiceManager : NSObject

@property(nonatomic, weak) id<AWSServiceManagerDelegate> delegate;

+ (instancetype)managerWithDelegate:(id<AWSServiceManagerDelegate>)delegate;
- (void)startUpdateWithCategory:(NSString *)category;

@end
