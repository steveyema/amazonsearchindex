//
//  SettingsViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingTableViewCell.h"
#import "PickerViewController.h"
#import "SettingController.h"

typedef NS_ENUM(NSInteger, SettingEntry) {
    CategoryEntry = 0,
    CurrentyEntry,
    SectionCount,
};


@interface SettingsViewController ()<UITableViewDataSource, UITableViewDelegate, SettingsViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)doneClicked:(id)sender;

@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    PickerViewController *pickerViewController = (PickerViewController*)[segue destinationViewController];
    pickerViewController.delegate = self;
}

+ (NSString *)uniqueIdentifier
{
    return @"settingTableViewCellIdentifier";
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[SettingsViewController uniqueIdentifier]];

    switch (indexPath.row)
    {
        case CategoryEntry:
        {
            cell.settingTitle.text = [NSString stringWithFormat:@"Category - %@", [SettingController sharedInstance].category];
            break;
        }
        default:
        {
            cell.settingTitle.text = [NSString stringWithFormat:@"Currency - %@", [SettingController sharedInstance].currency];
            break;
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = indexPath.row;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return SectionCount;
}

- (IBAction)doneClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(doneSetting)])
    {
        [self.delegate doneSetting];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PickerViewControllerDelegate
- (NSArray *)items
{
    NSArray *itemsToFill = nil;
    switch (self.selectedIndex) {
        case CategoryEntry:
            itemsToFill = [SettingController sharedInstance].categoryList;
            break;
            
        default:
            itemsToFill = [SettingController sharedInstance].currencyList;
            break;
    }
    
    return itemsToFill;
}

- (BOOL)itemSelectedWithIndex:(NSInteger)index
{
    NSString *selectedItem = nil;
    NSString *currentItem = nil;
    switch (self.selectedIndex) {
        case CategoryEntry:
        {
            selectedItem = (NSString *)[SettingController sharedInstance].categoryList[index];
            currentItem = [SettingController sharedInstance].category;
            break;
        }
        default:
        {
            selectedItem = (NSString *)[SettingController sharedInstance].currencyList[index];
            currentItem = [SettingController sharedInstance].currency;
            break;
        }
    }
    return selectedItem != nil && currentItem != nil && [selectedItem isEqualToString:currentItem];
}

- (void)selectItemWithIndex:(NSInteger)index
{
    switch (self.selectedIndex) {
        case CategoryEntry:
        {
            NSArray *items = [SettingController sharedInstance].categoryList;
            [SettingController sharedInstance].category = items[index];
            break;
        }
        default:
        {
            NSArray *items = [SettingController sharedInstance].currencyList;
            [SettingController sharedInstance].currency = items[index];
            break;
        }
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (IS_IPAD())
    {
        // Support all interface orientations.
        return UIInterfaceOrientationMaskAll;
    }
    
    else
    {
        // Make sure we only support portrait orientation.
        return UIInterfaceOrientationMaskPortrait;
    }
}



@end
