//
//  ProductViewController.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"

@class ProductDataSource;

@interface ProductViewController : UIViewController<SettingsViewControllerDelegate>

@property (nonatomic, strong) ProductDataSource *dataSource;

@end
