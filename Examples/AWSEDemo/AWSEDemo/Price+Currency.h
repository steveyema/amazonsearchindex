//
//  Price+Currency.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "Price.h"

@interface Price (Currency)

- (NSString *)formattedPriceForCurrencyConversion:(NSNumber*)currencyRate;

@end
