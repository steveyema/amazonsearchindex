//
//  PickerViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "PickerViewController.h"
#import "SettingTableViewCell.h"
#import "SettingController.h"

@interface PickerViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *items;

@end

@implementation PickerViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[PickerViewController uniqueIdentifier]];
    
    NSArray *items = self.items;
    
    if (items && indexPath.row < items.count)
    {
        cell.settingTitle.text = items[indexPath.row];
    }
    
    if ([self.delegate respondsToSelector:@selector(itemSelectedWithIndex:)])
    {
        [cell setSelected:[self.delegate itemSelectedWithIndex:indexPath.row] animated:NO];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *items = self.items;
    
    if (items && indexPath.row < items.count)
    {
        if ([self.delegate respondsToSelector:@selector(selectItemWithIndex:)])
        {
            [self.delegate selectItemWithIndex:indexPath.row];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (NSArray *)items
{
    if ([self.delegate respondsToSelector:@selector(items)])
    {
        NSArray *items = [self.delegate items];
        return items;
    }
    else
    {
        return nil;
    }
}


@end
