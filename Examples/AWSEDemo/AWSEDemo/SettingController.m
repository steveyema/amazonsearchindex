//
//  SettingController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "SettingController.h"
#import "CurrencyController.h"
#import "CategoryController.h"

#define CURRENT_SETTING_KEY @"CurrentSettings"
#define CATEGORY_DICTIONARY_KEY @"category"
#define CURRENT_DICTIONARY_KEY @"currency"

@interface SettingController()

@property (nonatomic, strong) NSDictionary *currentSettings;
@property (nonatomic, strong) NSArray *categoryList;
@property (nonatomic, strong) NSArray *currencyList;

@end

@implementation SettingController

+ (instancetype)sharedInstance
{
    static SettingController *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SettingController alloc] init];
        
    });
    
    return manager;
}

#pragma mark - getter/setter
- (NSArray *)categoryList
{
    if (!_categoryList)
    {
        _categoryList = [CategoryController categoryList];
    }
    return _categoryList;
}

- (NSArray *)currencyList
{
    if (!_currencyList)
    {
        _currencyList = [CurrencyController currencyList];
    }
    return _currencyList;
}

- (NSDictionary*)currentSettings
{
    if (!_currentSettings)
    {
        _currentSettings = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_SETTING_KEY];
    }
    
    if (!_currentSettings)
    {
        NSArray *currencyList = [CurrencyController currencyList];
        NSArray *categoryList = [CategoryController categoryList];
        _currentSettings = @{CATEGORY_DICTIONARY_KEY : [categoryList firstObject], CURRENT_DICTIONARY_KEY : [currencyList firstObject]};
        
    }
    
    return _currentSettings;
}

- (NSString *)category
{
    return [self.currentSettings valueForKey:CATEGORY_DICTIONARY_KEY];
}

- (NSString *)currency
{
    return [self.currentSettings valueForKey:CURRENT_DICTIONARY_KEY];
}

- (void)setCategory:(NSString *)category
{
    NSMutableDictionary *settings = [self.currentSettings mutableCopy];
    [settings setObject:category forKey:CATEGORY_DICTIONARY_KEY];
    _currentSettings = [settings copy];
    [self setObject:settings forKey:CURRENT_SETTING_KEY];
}

- (void)setCurrency:(NSString *)currency
{
    NSMutableDictionary *settings = [self.currentSettings mutableCopy];
    [settings setObject:currency forKey:CURRENT_DICTIONARY_KEY];
    _currentSettings = [settings copy];
    [self setObject:settings forKey:CURRENT_SETTING_KEY];
}

- (void)setObject:(id)object forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
