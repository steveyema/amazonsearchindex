//
//  SettingTableViewCell.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *settingTitle;

@end
