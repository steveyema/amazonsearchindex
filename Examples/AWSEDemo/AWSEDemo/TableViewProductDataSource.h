//
//  TableViewProductDataSource.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductDataSource.h"

@interface TableViewProductDataSource : ProductDataSource<UITableViewDelegate, UITableViewDataSource>

- (id)initWithTableView:(UITableView *)tableView;

@end
