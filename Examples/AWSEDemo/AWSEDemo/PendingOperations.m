//
//  PendingOperations.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "PendingOperations.h"

@implementation PendingOperations

- (id)init
{
    if (self = [super init])
    {
        _queue = [[NSOperationQueue alloc] init];
        _queue.name = @"Download Queue";
        _queue.maxConcurrentOperationCount = 1;
        _downloadsInProgress = [[NSMutableDictionary alloc] init];
    }
    return self;
}

@end
