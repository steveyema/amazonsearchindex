//
//  TableViewProductDataSource.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "TableViewProductDataSource.h"
#import "ProductTableViewCell.h"
#import "Price+Currency.h"
#import "CurrencyController.h"
#import "SettingController.h"
#import "CommonTypes.h"
#import "Product.h"

@interface TableViewProductDataSource()

@property(nonatomic, strong) UITableView *tableView;

@end

@implementation TableViewProductDataSource

- (id)initWithTableView:(UITableView *)tableView
{
    if (self = [super init])
    {
        _tableView = tableView;
        
        [tableView registerNib: [UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:[ProductTableViewCell unqiueIdentifier]];
        tableView.delegate = self;
        tableView.dataSource = self;
    }
    return self;
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items? self.items.count : 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductTableViewCell *cell = (ProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[ProductTableViewCell unqiueIdentifier]];
    
    [self setCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)setCell:(ProductTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Product *product = (Product *)[self.items objectAtIndex:indexPath.row];
    cell.productName.text = product.item.itemAttributes.title;
    
    Price *price = product.item.itemAttributes.listPrice;
    cell.productPrice.text = [price formattedPriceForCurrencyConversion:[[CurrencyController sharedInstance] currentRateFromUSDTo:[SettingController sharedInstance].currency]];
    
    cell.imageView.image = product.image;
    
    if (cell.accessoryView == nil)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.accessoryView = indicator;
    }
    
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)cell.accessoryView;
    
    switch (product.state) {
        case PhotoRecordStateDownloaded:
        {
            [indicator stopAnimating];
            break;
        }
        case PhotoRecordStateFailed:
        {
            [indicator stopAnimating];
            break;
        }
        default:
        {
            if (!self.tableView.dragging && !self.tableView.decelerating)
            {
                [self startOperationsForPhotoRecord:product atIndexPath:indexPath];
            }
            break;
        }
    }

}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 50.0;
}

#pragma mark - abstract implementation
- (void)updateUI
{
    [self.tableView reloadData];
}

- (void)reloadRowAtIndexPath:(NSIndexPath *)indexPath
{
    //do not implement
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (NSURL *)imageUrl:(Item *)item
{
    return [NSURL URLWithString:item.smallImage.url];
}

@end
