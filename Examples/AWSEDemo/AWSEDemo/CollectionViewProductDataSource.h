//
//  CollectionViewProductDataSource.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductDataSource.h"

@interface CollectionViewProductDataSource : ProductDataSource<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

- (id)initWithCollectionView:(UICollectionView *)collectionView;

@end
