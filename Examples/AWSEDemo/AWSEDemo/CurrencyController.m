//
//  CurrencyController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "CurrencyController.h"

#define CURRENCY_CONVERSION_INFO @"CURRENCY_CONVERSION_INFO"

@interface CurrencyController()

@property(nonatomic, strong) NSURLSession *defaultSession;
@property(nonatomic, strong) NSURLSessionDataTask *dataTask;
@property(nonatomic, strong) NSDictionary *currencyConversionInfo;

@end

@implementation CurrencyController

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static CurrencyController *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[CurrencyController alloc] init];
    });
    return manager;
}

+ (NSArray *)currencyList
{
    static NSArray *list = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        list = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CURRENCYLIST"];
    });
    return list;
}

+ (NSString *)APIKey
{
    static NSString *key = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        key = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"APILAYER_KEY"];
    });
    return key;
}

- (id)init
{
    if (self = [super init])
    {
        _defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (void)startUpdate
{
    if (self.dataTask)
    {
        [self.dataTask cancel];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:Notification_Currency_Start_Update object:nil];
    
    NSString *currencyListCommaSeperatedString = [[CurrencyController currencyList] componentsJoinedByString:@","];
    NSString *urlString = [NSString stringWithFormat:@"http://www.apilayer.net/api/live?access_key=%@&currencies=%@", [CurrencyController APIKey], currencyListCommaSeperatedString];
    self.dataTask = [self.defaultSession dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:Notification_Currency_Start_Update object:nil];
        });
        
        if (error)
        {
            NSLog(@"%@", error.localizedDescription);
        }
        else
        {
            if ([response isKindOfClass:[NSHTTPURLResponse class]])
            {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if (httpResponse.statusCode == 200)
                {
                    [self updateResult:data];
                }
            }
        }
    }];
    [self.dataTask resume];
}

- (void)updateResult:(NSData *)responseData
{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                         options:kNilOptions
                                                           error:&error];
    
    if (!error)
    {
        [[NSUserDefaults standardUserDefaults] setValue:json forKey:CURRENCY_CONVERSION_INFO];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:Notification_Currency_Updated object:nil];
    });
}


#pragma mark - persistence
- (NSNumber *)currentRateFromUSDTo:(NSNumber *)toCurrency
{
    NSDictionary *currencyConversionInfo = self.currencyConversionInfo;
    NSDictionary *quote = [currencyConversionInfo objectForKey:@"quotes"];
    if (quote)
    {
        NSString *key = [NSString stringWithFormat:@"USD%@", toCurrency];
        return [quote valueForKey:key];
    }
    else
    {
        return nil;
    }
}

#pragma mark - getter/setter
- (NSDictionary *)currencyConversionInfo
{
    if (!_currencyConversionInfo)
    {
        _currencyConversionInfo = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENCY_CONVERSION_INFO];
    }
    
    return _currencyConversionInfo;
}

@end
