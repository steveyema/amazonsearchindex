//
//  ProductDataSource.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductDataSource.h"
#import "TableViewProductDataSource.h"
#import "CollectionViewProductDataSource.h"
#import "AWSServiceManager.h"
#import "Product.h"
#import "CommonTypes.h"
#import "PendingOperations.h"
#import "ImageDownloader.h"
#import "SettingController.h"

@interface ProductDataSource()<AWSServiceManagerDelegate>

@property(nonatomic, strong) AWSServiceManager *awsServiceManager;
@property(nonatomic, strong) PendingOperations *pendingOperations;

@end

@implementation ProductDataSource

+ (instancetype)tableViewProductDataSourceWithTableView:(UITableView*)tableView
{
    return [[TableViewProductDataSource alloc] initWithTableView:tableView];
}

+ (instancetype)collectionViewProductDataSourceWithCollectionView:(UICollectionView *)collectionView
{
    return [[CollectionViewProductDataSource alloc] initWithCollectionView:collectionView];
}

- (id)init
{
    if (self = [super init])
    {
        _awsServiceManager = [AWSServiceManager managerWithDelegate:self];
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return self;
}

- (void)startUpdate
{
    [self.awsServiceManager startUpdateWithCategory:[[SettingController sharedInstance] category]];
}


#pragma mark - abstract methods
- (void)updateUI
{
    //do not implement
}

- (void)reloadRowAtIndexPath:(NSIndexPath *)indexPath
{
    //do not implement
}

- (NSURL *)imageUrl:(Item *)item
{
    //do not implement
    return nil;
}

- (void)startOperationsForPhotoRecord:(Product *)product atIndexPath:(NSIndexPath *)indexPath
{
    if (product.state == PhotoRecordStateNew)
    {
        ImageDownloader *downloader = (ImageDownloader *)[self.pendingOperations.downloadsInProgress objectForKey:indexPath];
        if (downloader)
        {
            return;
        }
        
        downloader = [[ImageDownloader alloc] initWithPhotoRecord:product];
        
        __weak ImageDownloader *weakDownloader = downloader;
        downloader.completionBlock = ^{
            if (weakDownloader.cancelled)
            {
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
                
                [self reloadRowAtIndexPath:indexPath];
            });
        };
        
        self.pendingOperations.downloadsInProgress[indexPath] = downloader;
        [self.pendingOperations.queue addOperation:downloader];
    }
}

#pragma mark - AWSServiceManagerDelegate
-(void)readyWithListOfItems:(NSArray *)items
{
    NSMutableArray *mutableItems = [[NSMutableArray alloc] initWithCapacity:items.count];
    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //
        Item *item = (Item *)obj;
        Product *product = [[Product alloc] initWithItem:obj andURL:[self imageUrl:item]];
        [mutableItems addObject:product];
    }];
    self.items = [mutableItems copy];
    [self updateUI];
}


@end
