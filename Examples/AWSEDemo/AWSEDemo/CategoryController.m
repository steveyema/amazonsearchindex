//
//  CategoryController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "CategoryController.h"

@implementation CategoryController

+ (NSArray *)categoryList
{
    static dispatch_once_t categoryOnceToken;
    static NSArray *list;
    dispatch_once(&categoryOnceToken, ^{
        list = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CATEGORYLIST"];
    });
    return list;
}

@end
