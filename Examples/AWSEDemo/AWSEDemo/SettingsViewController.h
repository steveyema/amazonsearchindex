//
//  SettingsViewController.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewControllerDelegate <NSObject>

@optional
- (NSArray *)items;
- (void)selectItemWithIndex:(NSInteger)index;
- (BOOL)itemSelectedWithIndex:(NSInteger)index;
- (void)doneSetting;

@end

@interface SettingsViewController : UIViewController

+ (NSString *)uniqueIdentifier;

@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;

@end
