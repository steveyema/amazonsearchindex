//
//  Constants.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define IS_IPAD() (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define Notification_Currency_Start_Update @"Notification_Currency_Start_Update"
#define Notification_Currency_End_Update @"Notification_Currency_End_Update"
#define Notification_Currency_Updated @"Notification_Currency_Updated"

static NSString *const AWSErrorDomain = @"ma.steve.AWSErrorDomain";

#endif