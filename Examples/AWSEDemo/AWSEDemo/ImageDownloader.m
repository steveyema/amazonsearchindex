//
//  ImageDownloader.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ImageDownloader.h"
#import "PhotoRecord.h"

@interface ImageDownloader()

@property (nonatomic, strong) PhotoRecord *photoRecord;

@end

@implementation ImageDownloader

- (id)initWithPhotoRecord:(PhotoRecord *)photoRecord
{
    if (self = [super init])
    {
        _photoRecord = photoRecord;
    }
    return self;
}

- (void)main
{
/*
 if self.cancelled {
 return
 }
 
 let imageData = NSData(contentsOfURL: self.photoRecord.url)
 
 if self.cancelled {
 return
 }
 
 if imageData?.length > 0 {
 self.photoRecord.image = UIImage(data: imageData!)
 self.photoRecord.state = .Downloaded
 }
 else {
 self.photoRecord.image = UIImage(named: "Failed")
 self.photoRecord.state = .Failed
 
 }

 */
    
    if (self.cancelled)
    {
        return;
    }
    
    NSData *imageData = [NSData dataWithContentsOfURL:self.photoRecord.url];
    
    if (self.cancelled)
    {
        return;
    }
    
    if (imageData && imageData.length > 0)
    {
        self.photoRecord.image = [UIImage imageWithData:imageData];
        self.photoRecord.state = PhotoRecordStateDownloaded;
    }
    else
    {
        self.photoRecord.image = [UIImage imageNamed:@"Failed"];
        self.photoRecord.state = PhotoRecordStateFailed;
    }
}

@end
