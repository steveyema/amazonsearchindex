//
//  ProductDataSource.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Product;

@interface ProductDataSource : NSObject

@property (nonatomic, strong) NSArray *items;

+ (instancetype)tableViewProductDataSourceWithTableView:(UITableView*)tableView;
+ (instancetype)collectionViewProductDataSourceWithCollectionView:(UICollectionView *)collectionView;
- (void)startOperationsForPhotoRecord:(Product *)product atIndexPath:(NSIndexPath *)indexPath;
- (void)startUpdate;
- (void)updateUI;

@end
