//
//  AWSServiceManager.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "AWSServiceManager.h"
#import "AWSECommerceServiceClient.h"
#import "CommonTypes.h"
#import "SOAP11Fault.h"
#import "SettingController.h"

@implementation AWSServiceManager

+ (instancetype)managerWithDelegate:(id<AWSServiceManagerDelegate>)delegate
{
    return [[AWSServiceManager alloc] initWithDelegate:delegate];
}

- (id)initWithDelegate:(id<AWSServiceManagerDelegate>)delegate
{
    if (self = [super init])
    {
        self.delegate = delegate;
    }
    return self;
}

- (void)startUpdateWithCategory:(NSString *)category
{
    AWSECommerceServiceClient *client = [AWSECommerceServiceClient sharedClient];
    
    // build request, see details here:
    ItemSearch *request = [[ItemSearch alloc] init];
    request.associateTag = @"tag"; // seems any tag is ok
    request.shared = [[ItemSearchRequest alloc] init];
    request.shared.searchIndex = category;
    request.shared.responseGroup = [NSMutableArray arrayWithObjects:@"Images", @"ItemAttributes", nil];
    ItemSearchRequest *itemSearchRequest = [[ItemSearchRequest alloc] init];
    itemSearchRequest.keywords = @"Adam";
    request.request = [NSMutableArray arrayWithObject:itemSearchRequest];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    // authenticate the request
    // http://docs.aws.amazon.com/AWSECommerceService/latest/DG/NotUsingWSSecurity.html
    [client authenticateRequest:@"ItemSearch"];
    [client itemSearch:request success:^(ItemSearchResponse *responseObject) {
        // stop progress activity
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        
        [self handleResponse:responseObject withErrorString:nil];

    } failure:^(NSError *error, id<PicoBindable> soapFault) {

        NSString *err = @"Unknown error";

        if (error)
        {
            err = error.localizedDescription;
        } else if (soapFault) {
            
            SOAP11Fault *soap11Fault = (SOAP11Fault *)soapFault;
            err = soap11Fault.faultstring;
        }
        
        [self handleResponse:nil withErrorString:err];
    }];
    

}

- (void)handleResponse:(ItemSearchResponse *)responseObject withErrorString:(NSString *)errorString
{
    if (errorString)
    {
        //show error message
        NSLog(@"show error message");
    }
    else
    {
        if (responseObject.items.count > 0)
        {
            Items *items = [responseObject.items objectAtIndex:0];
            if (items.item.count > 0)
            {
                if ([self.delegate respondsToSelector:@selector(readyWithListOfItems:)])
                {
                    [self.delegate readyWithListOfItems:items.item];
                }
            }
            else
            {
                //show no result
                NSLog(@"show no result");
            }
            
        }
        else
        {
            //show no result
            NSLog(@"show no result");
        }
    }
}

@end
