//
//  SettingController.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingController : NSObject

@property (nonatomic, readonly) NSArray *categoryList;
@property (nonatomic, readonly) NSArray *currencyList;
@property (nonatomic, readonly) NSDictionary *currentSettings;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *currency;

+ (instancetype)sharedInstance;

@end
