//
//  PhotoRecord.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PhotoRecordState) {
    PhotoRecordStateNew,
    PhotoRecordStateDownloaded,
    PhotoRecordStateFailed
};

@interface PhotoRecord : NSObject

@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, assign) PhotoRecordState state;

-(id)initWithUrl:(NSURL *)url;

@end


