//
//  PhotoRecord.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "PhotoRecord.h"

@implementation PhotoRecord

-(id)initWithUrl:(NSURL *)url
{
    if (self = [super init])
    {
        _image = [UIImage imageNamed:@"place_holder"];
        _url = url;
    }
    return self;
}

@end
