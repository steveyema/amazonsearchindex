//
//  ViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductSelectionViewController.h"
#import "TableViewProductViewController.h"
#import "CollectionViewProductViewController.h"
#import "ProductDataSource.h"
#import "SettingsViewController.h"

@interface ProductSelectionViewController ()

@property(nonatomic, strong) ProductDataSource *dataSource;
@property(nonatomic, strong) ProductViewController *productionViewController;

@end

@implementation ProductSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self loadProductionViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadProductionViewController
{
    // We'll store a reference to whichever view controller we decide to load.
    ProductViewController *productViewController = nil;
    
    if (IS_IPAD())
    {
        productViewController = [[CollectionViewProductViewController alloc] initWithNibName:@"ProductView" bundle:nil];
    }
    else
    {
        productViewController = [[TableViewProductViewController alloc] initWithNibName:@"ProductView" bundle:nil];
    }
    
    self.productionViewController = productViewController;

//    // Set the loaded view controller as our subview.
    [self.view addSubview:productViewController.view];
//
//    // Resize the loaded view controller's view frame to match our own.
    productViewController.view.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
//
//    // Finally, set the new view controller as our child.
    [self addChildViewController:productViewController];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    SettingsViewController *settingsViewController = (SettingsViewController*)navController.topViewController;
    settingsViewController.delegate = self.productionViewController;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    UIViewController *topViewController = nil;
    
    // Do we have any valid child View Controllers?
    if (self.childViewControllers.count > 0)
    {
        // Grab the top child View Controller.
        topViewController = [self.childViewControllers objectAtIndex:0];
        
        // Does our top View Controller handle the -supportedInterfaceOrientations call?
        if ([topViewController respondsToSelector:@selector(supportedInterfaceOrientations)])
            // Return the top View Controller's call version of this method.
            return [topViewController supportedInterfaceOrientations];
        
        // Otherwise, our top View Controller doesn't support the method.
        else
            // Just use our super's implementation.
            return [super supportedInterfaceOrientations];
    }
    else
    {
        // Just use our super's implementation.
        return [super supportedInterfaceOrientations];
    }
}

@end
