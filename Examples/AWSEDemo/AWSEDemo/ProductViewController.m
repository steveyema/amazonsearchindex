//
//  ProductViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-29.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductDataSource.h"
#import "CurrencyController.h"
#import "SettingController.h"

@interface ProductViewController ()

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupObservers];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (IS_IPAD())
    {
        // Support all interface orientations.
        return UIInterfaceOrientationMaskAll;
    }
    
    else
    {
        // Make sure we only support portrait orientation.
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)dealloc
{
    [self removeObservers];
}

- (void)setupObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currencyConversionUpdated:) name:Notification_Currency_Updated object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)currencyConversionUpdated:(id)notification
{
    NSString *currency = [SettingController sharedInstance].currency;
    NSString *convertedCurrency = [[CurrencyController sharedInstance] currentRateFromUSDTo:currency];
    
    NSLog(@"%@:%@", currency, convertedCurrency);
}

@end
