//
//  ProductViewNavigationController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ProductViewNavigationController.h"

@interface ProductViewNavigationController ()

@end

@implementation ProductViewNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    // Does our top View Controller handle the -supportedInterfaceOrientations call?
    if ([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)])
    {
        // Return the top View Controller's call version of this method.
        return [self.topViewController supportedInterfaceOrientations];
    }
    // Otherwise, our top View Controller doesn't support the method.
    else
    {
        if (IS_IPAD())
        {
            // Support all interface orientations.
            return UIInterfaceOrientationMaskAll;
        }
        
        else
        {
            // Make sure we only support portrait orientation.
            return UIInterfaceOrientationMaskPortrait;
        }
    }
}

@end
