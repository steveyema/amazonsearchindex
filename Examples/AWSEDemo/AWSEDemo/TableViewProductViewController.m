//
//  TableViewProductViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "TableViewProductViewController.h"
#import "ProductDataSource.h"
#import "CurrencyController.h"

@interface TableViewProductViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TableViewProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dataSource = [ProductDataSource tableViewProductDataSourceWithTableView:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[CurrencyController sharedInstance] startUpdate];
    [self.dataSource startUpdate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
