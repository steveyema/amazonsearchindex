//
//  Price+Currency.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "Price+Currency.h"

@implementation Price (Currency)

- (NSString *)formattedPriceForCurrencyConversion:(NSNumber*)currencyRate
{
    NSInteger price = [self.amount integerValue];
    double convertedPrice = price * [currencyRate doubleValue] / 100;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    return [numberFormatter stringFromNumber:[NSNumber numberWithFloat:convertedPrice]];
}

@end
