//
//  CollectionViewProductViewController.m
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "CollectionViewProductViewController.h"
#import "ProductDataSource.h"
#import "SettingsViewController.h"
#import "CurrencyController.h"

@interface CollectionViewProductViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CollectionViewProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dataSource = [ProductDataSource collectionViewProductDataSourceWithCollectionView:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[CurrencyController sharedInstance] startUpdate];
    [self.dataSource startUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - SettingsViewControllerDelegate
- (void)doneSetting
{
    NSLog(@"doneSetting");
    [[CurrencyController sharedInstance] startUpdate];
    [self.dataSource startUpdate];
    
}



@end
