//
//  ImageDownloader.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PhotoRecord;

@interface ImageDownloader : NSOperation

- (id)initWithPhotoRecord:(PhotoRecord *)photoRecord;

@end
