//
//  Product.h
//  AWSEDemo
//
//  Created by Ye Ma on 2016-05-30.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "PhotoRecord.h"


@class Item;
@interface Product : PhotoRecord

@property (nonatomic, strong) Item *item;

-(id)initWithItem:(Item *)item andURL:(NSURL *)url;

@end
