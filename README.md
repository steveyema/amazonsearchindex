Key notes:

ProductSelectionViewController -> ProductionViewController support different presentation by using device specific nib file
        ->ProductView~iphone    UITableView
        ->ProductView~iPad      UICollectionView

ProductDataSource abstract the data source management while TableViewProductDataSource & CollectionViewProductDataSource response to specific tableview/collection delegate callbacks.

PickerViewController inherit from SettingsViewController
With SettingsViewController as the base class share common table view and done button UI design

SettingsViewController present popover in iPhone and iPad

Orientation support Portrait only in iPhone

SettingsViewController driven by static data
PickerViewController driven by dynamic data through delegate

SettingsManager singleton serves as the data driven model for current settings.

CurrencyConvertManager singleton managed the currency convert live updates

Pico the third party library that provide network funcationaity for Amazon ecommerce service.

iPad UICollectionView 